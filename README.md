# DEVELOPER TEST - HTML/CSS/JS/PHP/MYSQL - v1.0

### Prerequisites:
    - This exercise will take 3-4 hours, please schedule your time ahead, get a cup of coffee or tea
    - This exercise is open-book style, feel free to use Google as much as you can but don't ask for external help, we know it
    - Setup basic php dev environment - including PHP 5.6 or later, MySQL 5 or newer and IDE of your favourites
    - We assume you know Git, so setup Git and config it properly

### Acceptance criteria:
    - The code must contain file extensions for php, css, js or sql, don't upload other irrelevant files (Hint: use .gitignore to control)
    - The code must be runnable, we just need to execute sql script and the php script should be working in modern browsers
    - You must provide some sample data in the sql file

### How we evaluate your work:
    - How much you can satisfy the core requirement and
    - We check the code quality, code style, code standard, code readability
    - We check the way you structure the project

### Exercise Requirement:
    Coeus Computer hires you to develop a web-based interface to effectively manage their staff information. Their user information contains
    full name, email, password, and mobile phone number. Each user can belong to one or many groups so Coeus can easily manage them, for example
    staff A is member of group Admin and group Operator. Each group can have many users.

##### Requirement #1:
    Develop a database model to store information of user and group. Add "Admin" and "Operator" as two main default groups

##### Requirement #2:
    Develop a web interface using PHP/HTML/CSS/JS which allows Coeus to add user information. The interface should allow Coeus to specify
    which group the user should belong to as well (for example they can choose a drop down box to select groups). The group drop down box
    must display all the groups from database. Furthemore, the form must not allow Coeus staff to enter invalid email format and invalid
    mobile phone number (Australia mobile phone number standard)

##### Requirement #3:
    Develop a web interface to display list of all users along with associated group. The list should have following columns: user id, email,
    full name, group name. The list is sorted from A to Z by full name. The list has a delete button on each row which allow Coeus staff to
    delete a user. When the staff clicks on Delete button, the page should ask for their confirmation by display "Are you sure?" box.

##### Requirement #4:
    In the list of user above. Coeus staff wants to know how many user has been deleted from the list. Display a message at bottom of the list showing
    that information. Upon deletion of any user the message should be reflected as well:
        i.e: Number of deleted user: 3

---
:) Happy coding






